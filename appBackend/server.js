var express=require('express');
var app = express();

var bodyParser=require('body-parser');

var userRouter=require('./backend/router/UserRouter');
var contactRouter=require('./backend/router/ContactRouter');

//Set up mongoose connection
var mongoose = require('mongoose');
var mongoDB = 'mongodb://127.0.0.1/contact';
mongoose.connect(mongoDB, {
  useMongoClient: true
});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.urlencoded({
    extended:true
}));

app.use(bodyParser.json());


app.use('/api',userRouter);
app.use('/api',contactRouter);

app.listen(5050);




