var mongoose = require('mongoose');
var Schema=mongoose.Schema;

var UserSchema = new mongoose.Schema({
  firstname: String,
  lastname: String,
  email: { type : String , unique : true, required : true },
  password: String,
  contacts: [{ type: Schema.Types.ObjectId, ref: 'Contact' }]
}, {timestamps: true});

var ContactSchema=new mongoose.Schema({
    firstname:String,
    lastname:String,
    phone:String,
    email:String,
    service:String,
    ville:String,
    user : { type: Schema.Types.ObjectId, ref: 'User' },
})

module.exports.user = mongoose.model('User', UserSchema);

module.exports.contact = mongoose.model('Contact', ContactSchema);