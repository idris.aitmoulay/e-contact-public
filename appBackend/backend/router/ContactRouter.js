var express = require('express');
var router = express.Router();
var userController=require('../controller/UserController');

var contactController=require('../controller/ContactController');


router.post('/contact/all',contactController.getContact);

router.post('/contact',contactController.postContact);

router.put('/contact',contactController.putContact);

router.delete('/contact',contactController.deleteContact);


module.exports = router;