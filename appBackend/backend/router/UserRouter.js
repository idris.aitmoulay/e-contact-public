var express = require('express');
var router = express.Router();
var userController=require('../controller/UserController');


router.get('/users',userController.getUser);

router.post('/user',userController.postUsers);

router.post('/user/password',userController.forgetPasssword);

router.post('/user/login',userController.getUsers);

router.put('/user',userController.updateUsers);

//router.delete('/user',userController.deleteUsers);


module.exports = router;