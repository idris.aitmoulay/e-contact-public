const nodemailer = require('nodemailer');

let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
        user: 'YOUR EMAIL', // il faut mettre votre email
        pass: 'YOUR PASSWORD'  // mettez votre password
    },
    tls:{
      rejectUnauthorized:false // on la met pour pass d envoie d'email via localhost
    }
});

let mailOptions = {
    from: '"e-Contact" <idris.aitmoulay@gmail.com>', // l'email de l'application (autrement dit de l'entreprise nommée e-contact)
    to: '', // list of receivers
    subject: 'Forget Password', // Subject line
    text: 'your password', // plain text body
    html: 'output' // html body
};


exports.sendEmail=function(email,output,callback){
    mailOptions.to=email;
    mailOptions.html=output;
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error)
            callback(false);
            return;
        }
        console.log('Message sent: %s', info.messageId);   
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        callback(true);
    });
}