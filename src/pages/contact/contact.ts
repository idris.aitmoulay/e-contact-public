import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest'; 


/**
 * Generated class for the ContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {
  public contact:any;

  public firstname:string;
  public lastname:string;
  public email:string;
  public phone:string;
  public service:string;
  public ville:string;

  constructor(public navCtrl: NavController, public navParams: NavParams,public rest:RestProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
    this.contact=this.navParams.get('body');

    this.dataBindingContact();
  }

  onUpdateContact(){
    this.rest.updateContact({_id:this.contact._id,firstname:this.firstname,
                              lastname:this.lastname,email:this.email, phone:this.phone,
                            ville:this.ville,service:this.service}).then((resulta)=>{
      this.contact=resulta;
      this.dataBindingContact();
      console.log(resulta);
    },(err)=>{
      console.log(err)
    })
  }

  dataBindingContact(){
    this.firstname=this.contact.firstname;
    this.lastname=this.contact.lastname;
    this.email=this.contact.email;
    this.phone=this.contact.phone;
    this.ville=this.contact.ville;
    this.service=this.contact.service;
  }

}
