import { Component } from '@angular/core';
import { NavController , LoadingController} from 'ionic-angular';
import { Toast } from '@ionic-native/toast';

import { RestProvider } from '../../providers/rest/rest';


@Component({
  selector: 'page-about',
  templateUrl: 'signup.html'
})
export class SignUpPage {

  public firstname:string;
  public lastname:string;
  public email:string;
  public password:string;
  public repeatpassword:string;

  public error:string="";

  public person:any;
  loading: any;


  constructor(public navCtrl: NavController,public toast:Toast, public loadingCtrl: LoadingController,public rest: RestProvider) {

  }
  
//inscription databinding existe
  logForm(){
    if(this.firstname!=null && this.lastname!=null && this.email!=null && this.password!=null && this.repeatpassword!=null){
      if(this.password === this.repeatpassword){
        this.rest.register({firstname:this.firstname,lastname:this.lastname,email:this.email,password:this.password}).then((resulta)=> {
          console.log(resulta)
          this.person = resulta;
          if(this.person.succes==true){
            this.person=this.person.user;
          }else{
            this.error="this username and email already existe!!";
          }
        },(err)=> {
          console.log(err)
        }); 
      }else{
        this.error="Password is not equal to Repeated Password";
      }
    }else{
      this.error="form incomplet";
    }
  }
}
