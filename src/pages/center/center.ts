import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController  } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import {ContactPage}  from '../../pages/contact/contact';
/**
 * Generated class for the CenterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-center',
  templateUrl: 'center.html',
})
export class CenterPage {
  public myInput:string;

  public contacts:any/*=[{name:"ahmed"},{name:"said"},{name:"said"},{name:"said"},{name:"said"},{name:"said"},{name:"said"},{name:"said"}]*/;

  public contactSource:any;
  private person:any;
  private radio:string="none";

  constructor(public navCtrl: NavController, public navParams: NavParams,public rest:RestProvider,public alertCtrl :AlertController) {
  }

  ionViewDidLoad() {
    this.person=this.rest.getSession()[0];
    this.rest.getContacts({ids:this.person.contacts}).then((resulta)=>{
      this.contacts=resulta;
      this.contactSource=resulta;
      this.contactSource=this.contactSource.contact;
      this.contacts=this.contacts.contact;
      console.log(resulta);
    },(err)=>{
      console.log(err)
    })
  }

  onInput(event){
    var tab=[];
    var i;
    switch(this.radio){
      case "none": 
      break;
      case "name": 
      for(i=0;i<this.contactSource.length;i++){
        if((this.contactSource[i].firstname).includes(this.myInput)){
          tab.push(this.contactSource[i]);
        }
      }
      this.contacts=tab;
      break;
      case "service": tab=[];
      for(i=0;i<this.contactSource.length;i++){
        if((this.contactSource[i].service).includes(this.myInput)){
          tab.push(this.contactSource[i]);
        }
      }
      this.contacts=tab; break;
      case "ville": 
      tab=[];
      for(i=0;i<this.contactSource.length;i++){
        if((this.contactSource[i].ville).includes(this.myInput)){
          tab.push(this.contactSource[i]);
        }
      }
      this.contacts=tab; break;
    }
  }

  onCancel(event){
    console.log("cancel")
  }

  open(event, item){
    console.log(item);
    let data = {
      title :'title',
      body : item
    };
    this.navCtrl.push(ContactPage,data);
  }
  addContact(){
    console.log("add contact")
    this.presentPrompt();
  }

  presentPrompt() {
    let alert = this.alertCtrl.create({
      title: 'new Contact',
      message : 'make sure to fill all the blanks',
      inputs: [
        {
          name: 'firstname',
          placeholder: 'Firstname'
        },
        {
          name: 'lastname',
          placeholder: 'Lastname',
        },
        {
          name: 'email',
          placeholder: 'Email',
        },
        {
          name: 'phone',
          placeholder: 'Phone',
        },
        {
          name: 'service',
          placeholder: 'Service',
        },
        {
          name: 'ville',
          placeholder: 'ville',
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Add',
          handler: data => {
            if(data.firstname!=null && data.lastname!=null && data.email!=null && data.phone!=null && data.service!=null && data.ville !=null){
              this.rest.addContact({_id:this.person._id,firstname:data.firstname,
                                lastname:data.lastname,phone:data.phone,
                                email:data.email,service:data.service,
                                ville:data.ville}).then((resulta)=>{
                                  var list:any=resulta;
                                  this.rest.getContacts({ids:list.contacts}).then((resulta)=>{
                                    this.contacts=resulta;
                                    this.contactSource=resulta;
                                    this.contactSource=this.contactSource.contact;
                                    this.contacts=this.contacts.contact;
                                    console.log(resulta);
                                  },(err)=>{
                                    console.log(err)
                                  })
                                },(err)=>{
                                  console.log(err)
                                })
            }else{
              
              return false;
          }
          }
        }
      ]
    });
    alert.present();
  }

  nameSelect(event){
    this.radio="name";
  }

  serviceSelect(event){
    this.radio="service";
  }

  villeSelect(event){
    this.radio="ville";
  }
}
