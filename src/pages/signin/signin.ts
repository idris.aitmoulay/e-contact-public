import { Component } from '@angular/core';
import { App ,NavController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import {CenterPage} from '../center/center';

@Component({
  selector: 'page-contact',
  templateUrl: 'signin.html'
})
export class SignInPage {
  public error:string="";
  public person:any;

  public email:string;
  public password:string;

  constructor(public navCtrl: NavController,public rest: RestProvider,public app:App) {

  }

  logForm(){
    if(this.email!=null && this.password!=null ){
        this.rest.login({email:this.email,password:this.password}).then((resulta)=> {
          console.log(resulta)
          this.person = resulta;
          if(this.person.succes==true){
            this.person=this.person.user;
            this.rest.setSession(this.person);
            this.loadCenterPage()
          }else{
            this.error="your email or password are incorrect";
          }
        },(err)=> {
          console.log(err)
        }); 
    }else{
      this.error="form incomplet";
    }
  }
  loadCenterPage(){
    this.app.getRootNav().setRoot(CenterPage)
    this.navCtrl.setRoot(CenterPage);
  }

  forgetPassword(){
    if(this.email!=null){
      this.rest.forgetPassword({email:this.email}).then((res)=> {
        console.log("res")
      },(err)=> {
        console.log("err")
      })
    }else{
      this.error="remplir le champs email puis appuyer forget my password";
    }
  }
}
