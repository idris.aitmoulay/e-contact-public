import { Component } from '@angular/core';

import { SignUpPage } from '../signup/signup';
import { SignInPage } from '../signin/signin';
import { HomePage } from '../home/home';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = SignInPage;
  tab3Root = SignUpPage;

  constructor() {

  }

}
